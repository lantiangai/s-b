(function() {
  var _ref, _ref1, _ref2, _ref3, _ref4, _ref5, _ref6,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.PNControl = (function(_super) {
    __extends(PNControl, _super);

    function PNControl() {
      _ref = PNControl.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    PNControl.prototype.defaults = {
      controlName: "",
      controlValue: ""
    };

    PNControl.prototype.getDisplayName = function() {
      if (this.hasValidName()) {
        return this.get("controlName");
      }
    };

    PNControl.prototype.validate = function(attrs) {
      var errors;

      errors = [];
      if (attrs.controlName === "" || attrs.controlValue === "") {
        errors.push({
          attribute: 'control valid',
          message: "Control Name and Value must be valid"
        });
      }
      if (errors.length > 0) {
        return errors;
      } else {
        return null;
      }
    };

    PNControl.prototype.isSame = function(other) {
      if (this.get("controlName") !== "" && this.get("controlName") === other.get("controlName")) {
        return true;
      }
      if (this.get("controlName") === "" && other.get("controlName") === "") {
        return true;
      }
      return false;
    };

    PNControl.prototype.hasValidValue = function() {
      return this.get("controlValue") !== "";
    };

    PNControl.prototype.hasValidName = function() {
      return this.get("controlName") !== "";
    };

    return PNControl;

  })(Backbone.Model);

  window.PNControlList = (function(_super) {
    __extends(PNControlList, _super);

    function PNControlList() {
      this.isValid = __bind(this.isValid, this);      _ref1 = PNControlList.__super__.constructor.apply(this, arguments);
      return _ref1;
    }

    PNControlList.prototype.model = PNControl;

    PNControlList.prototype.getValidBatchNames = function() {
      return this.filter(function(nm) {
        return nm.isValid();
      });
    };

    PNControlList.prototype.add = function(model, options) {
      var isDupe,
        _this = this;

      if (model instanceof Array) {
        return _.each(model, function(mdl) {
          return _this.add(mdl, options);
        });
      } else {
        isDupe = this.any(function(tmod) {
          return tmod.isSame(new PNControl(model));
        });
        if (isDupe) {
          return false;
        }
        return Backbone.Collection.prototype.add.call(this, model);
      }
    };

    PNControlList.prototype.isValid = function() {
      if ((this.getValidBatchNames().length === this.length) && (this.length !== 0)) {
        return true;
      } else {
        return false;
      }
    };

    PNControlList.prototype.clear = function() {
      return this.destroy();
    };

    return PNControlList;

  })(Backbone.Collection);

  window.PNControlController = (function(_super) {
    __extends(PNControlController, _super);

    function PNControlController() {
      this.updateControl = __bind(this.updateControl, this);
      this.deleteControl = __bind(this.deleteControl, this);
      this.render = __bind(this.render, this);      _ref2 = PNControlController.__super__.constructor.apply(this, arguments);
      return _ref2;
    }

    PNControlController.prototype.template = _.template($("#PNControlView").html());

    PNControlController.prototype.tagName = "tr";

    PNControlController.prototype.className = "pnControlView";

    PNControlController.prototype.events = {
      'change .bv_controlName': "updateControl",
      'change .bv_controlValue': "updateControl",
      'click .bv_controlButton': "deleteControl"
    };

    PNControlController.prototype.initialize = function() {
      this.model.on("change", this.render, this);
      return this.model.on("destroy", this.remove, this);
    };

    PNControlController.prototype.render = function() {
      $(this.el).html(this.template());
      this.$(".bv_controlName").val(this.model.get("controlName"));
      this.$(".bv_controlValue").val(this.model.get("controlValue"));
      if (!this.model.hasValidName()) {
        this.$('.bv_controlName').addClass("error");
      } else {
        this.$('.bv_controlName').removeClass("error");
      }
      if (!this.model.hasValidValue()) {
        this.$('.bv_controlValue').addClass("error");
      } else {
        this.$('.bv_controlValue').removeClass("error");
      }
      return this;
    };

    PNControlController.prototype.deleteControl = function() {
      console.log("click delete");
      return this.model.clear();
    };

    PNControlController.prototype.updateControl = function() {
      return this.model.set({
        controlName: this.$('.bv_controlName').find(":selected").text(),
        controlValue: $.trim(this.$('.bv_controlValue').val())
      });
    };

    return PNControlController;

  })(Backbone.View);

  window.PNControlListController = (function(_super) {
    __extends(PNControlListController, _super);

    function PNControlListController() {
      this.add = __bind(this.add, this);      _ref3 = PNControlListController.__super__.constructor.apply(this, arguments);
      return _ref3;
    }

    PNControlListController.prototype.template = _.template($("#PNControlListView").html());

    PNControlListController.prototype.initialize = function() {
      return this.collection.bind("add", this.add, this);
    };

    PNControlListController.prototype.render = function() {
      var _this = this;

      $(this.el).empty();
      $(this.el).html(this.template());
      this.collection.each(function(bName) {
        return _this.$('tbody').append(new PNControlController({
          model: bName
        }).render().el);
      });
      return this;
    };

    PNControlListController.prototype.add = function() {
      return this.render();
    };

    return PNControlListController;

  })(Backbone.View);

  window.EchoPlateMapper = (function(_super) {
    __extends(EchoPlateMapper, _super);

    function EchoPlateMapper() {
      _ref4 = EchoPlateMapper.__super__.constructor.apply(this, arguments);
      return _ref4;
    }

    EchoPlateMapper.prototype.defaults = {
      plateSize: 0,
      positiveControlList: null,
      negitiveControlList: null
    };

    EchoPlateMapper.prototype.validate = function(attrs) {
      var errors;

      errors = [];
      if (attrs.plateSize === "Insert Plate Size") {
        errors.push({
          attribute: 'plateSize',
          message: "Plate Size must be provided"
        });
      }
      if (errors.length > 0) {
        return errors;
      } else {
        return null;
      }
    };

    return EchoPlateMapper;

  })(Backbone.Model);

  window.EchoPlateMapperController = (function(_super) {
    __extends(EchoPlateMapperController, _super);

    function EchoPlateMapperController() {
      this.addNegitive = __bind(this.addNegitive, this);
      this.addPositive = __bind(this.addPositive, this);
      this.attributeChanged = __bind(this.attributeChanged, this);
      this.render = __bind(this.render, this);      _ref5 = EchoPlateMapperController.__super__.constructor.apply(this, arguments);
      return _ref5;
    }

    EchoPlateMapperController.prototype.template = _.template($("#EchoPlateMapperView").html());

    EchoPlateMapperController.prototype.events = {
      'change .bv_plateSize': "attributeChanged",
      'click .bv_addPositive': "addPositive",
      'click .bv_addNegitive': "addNegitive"
    };

    EchoPlateMapperController.prototype.initialize = function() {
      this.errorOwnerName = 'EchoPlateMapperController';
      $(this.el).html(this.template());
      this.positiveControlList = new PNControlListController({
        collection: new PNControlList(),
        el: this.$(".positiveControlContainer")
      });
      return this.negitiveControlList = new PNControlListController({
        collection: new PNControlList(),
        el: this.$(".negitiveControlContainer")
      });
    };

    EchoPlateMapperController.prototype.render = function() {
      return this;
    };

    EchoPlateMapperController.prototype.attributeChanged = function() {
      this.trigger('amDirty');
      return this.updateModel();
    };

    EchoPlateMapperController.prototype.updateModel = function() {
      return this.set({
        plateSize: this.getTrimmedInput('.plateSize')
      });
    };

    EchoPlateMapperController.prototype.addPositive = function() {
      event.preventDefault();
      this.positiveControlList.add(new PNControl());
      console.log("done");
      return console.log(this.positiveControlList);
    };

    EchoPlateMapperController.prototype.addNegitive = function() {
      event.preventDefault();
      return this.negitiveControlList.add(new PNControl());
    };

    return EchoPlateMapperController;

  })(AbstractFormController);

  window.EchoPlateMapperParserController = (function(_super) {
    __extends(EchoPlateMapperParserController, _super);

    function EchoPlateMapperParserController() {
      this.validateParseFile = __bind(this.validateParseFile, this);
      this.validateParseFile = __bind(this.validateParseFile, this);
      this.handleValidationReturnSuccess = __bind(this.handleValidationReturnSuccess, this);
      this.handleFPKFormInvalid = __bind(this.handleFPKFormInvalid, this);
      this.handleFPKFormValid = __bind(this.handleFPKFormValid, this);      _ref6 = EchoPlateMapperParserController.__super__.constructor.apply(this, arguments);
      return _ref6;
    }

    EchoPlateMapperParserController.prototype.initialize = function() {
      this.fileProcessorURL = "/api/echoPlateMapperParser";
      this.errorOwnerName = 'EchoPlateMapperParser';
      this.loadReportFile = false;
      EchoPlateMapperParserController.__super__.initialize.call(this);
      this.$('.bv_moduleTitle').html('Full PK Experiment Loader');
      this.fpkc = new EchoPlateMapperController({
        model: new EchoPlateMapper(),
        el: this.$('.bv_additionalValuesForm')
      });
      this.fpkc.on('valid', this.handleFPKFormValid);
      this.fpkc.on('invalid', this.handleFPKFormInvalid);
      this.fpkc.on('notifyError', this.notificationController.addNotification);
      this.fpkc.on('clearErrors', this.notificationController.clearAllNotificiations);
      return this.fpkc.render();
    };

    EchoPlateMapperParserController.prototype.handleFPKFormValid = function() {
      if (this.parseFileUploaded) {
        return this.handleFormValid();
      }
    };

    EchoPlateMapperParserController.prototype.handleFPKFormInvalid = function() {
      return this.handleFormInvalid();
    };

    EchoPlateMapperParserController.prototype.handleFormValid = function() {
      if (this.fpkc.isValid()) {
        return EchoPlateMapperParserController.__super__.handleFormValid.call(this);
      }
    };

    EchoPlateMapperParserController.prototype.handleValidationReturnSuccess = function(json) {
      EchoPlateMapperParserController.__super__.handleValidationReturnSuccess.call(this, json);
      return this.fpkc.disableAllInputs();
    };

    EchoPlateMapperParserController.prototype.showFileSelectPhase = function() {
      EchoPlateMapperParserController.__super__.showFileSelectPhase.call(this);
      if (this.fpkc != null) {
        return this.fpkc.enableAllInputs();
      }
    };

    EchoPlateMapperParserController.prototype.validateParseFile = function() {
      this.fpkc.updateModel();
      if (!!this.fpkc.isValid()) {
        this.additionalData = {
          inputParameters: this.fpkc.model.toJSON()
        };
        return EchoPlateMapperParserController.__super__.validateParseFile.call(this);
      }
    };

    EchoPlateMapperParserController.prototype.validateParseFile = function() {
      this.fpkc.updateModel();
      if (!!this.fpkc.isValid()) {
        this.additionalData = {
          inputParameters: this.fpkc.model.toJSON()
        };
        return EchoPlateMapperParserController.__super__.validateParseFile.call(this);
      }
    };

    return EchoPlateMapperParserController;

  })(BasicFileValidateAndSaveController);

}).call(this);
