(function() {
  describe('EchoPlateMapper Behavior Testing', function() {
    beforeEach(function() {
      return this.fixture = $.clone($('#fixture').get(0));
    });
    afterEach(function() {
      $('#fixture').remove();
      return $('body').append($(this.fixture));
    });
    describe('PNControl Model', function() {
      describe('when instantiated', function() {
        beforeEach(function() {
          return this.pnc = new PNControl();
        });
        return describe("defaults tests", function() {
          return it('should have defaults', function() {
            expect(this.pnc.get('controlName')).toEqual("");
            return expect(this.pnc.get('controlValue')).toEqual("");
          });
        });
      });
      return describe("validation tests", function() {
        beforeEach(function() {
          return this.pnc = new PNControl(window.EchoPlateMapperTestJSON.validPNControl);
        });
        it("should be valid as initialized", function() {
          return expect(this.pnc.isValid()).toBeTruthy();
        });
        return it('should have the controlName be "unassigned"', function() {
          var filtErrors;

          this.pnc.set({
            controlName: "",
            controlValue: ""
          });
          expect(this.pnc.isValid()).toBeFalsy();
          filtErrors = _.filter(this.pnc.validationError, function(err) {
            return err.attribute === 'control valid';
          });
          return expect(filtErrors.length).toBeGreaterThan(0);
        });
      });
    });
    describe("PNControlList Model", function() {
      return describe("when instantiated", function() {
        beforeEach(function() {
          return this.bnl = new PNControlList([
            {
              controlName: "reqName1",
              controlValue: "batchName1"
            }, {
              controlName: "reqName2",
              controlValue: "aliasName2"
            }, {
              controlName: "",
              controlValue: ""
            }
          ]);
        });
        it("should have three PNControl when instantiated with 3 models", function() {
          return expect(this.bnl.length).toEqual(3);
        });
        it("should return set list of valid models", function() {
          return expect(this.bnl.getValidBatchNames().length).toEqual(2);
        });
        it("should be invalid when has any invalid PNCotrol", function() {
          return expect(this.bnl.isValid()).toBeFalsy();
        });
        it("should not add models that are the same", function() {
          expect(this.bnl.length).toEqual(3);
          this.bnl.add({
            controlName: "reqName2",
            controlValue: "aliasName2"
          });
          this.bnl.add({
            controlName: "reqName2other",
            controlValue: "aliasName2"
          });
          return expect(this.bnl.length).toEqual(4);
        });
        return it("should be valid when all PNControl valid", function() {
          this.bnl.pop();
          return expect(this.bnl.isValid()).toBeTruthy();
        });
      });
    });
    describe("PNControl Controller", function() {
      return describe("when instantiated", function() {
        beforeEach(function() {
          this.bnc = new PNControlController({
            model: new PNControl({
              controlName: "reqName1",
              controlValue: "batchName1"
            }),
            el: this.fixture
          });
          return this.bnc.render();
        });
        it("should show the control name", function() {
          return expect(this.bnc.$(".bv_controlValue").val()).toEqual("batchName1");
        });
        it("should show an error if controlValue is not set", function() {
          this.bnc.$('.bv_controlValue').val("");
          this.bnc.$('.bv_controlValue').change();
          return expect(this.bnc.$('.bv_controlValue').hasClass('error')).toBeTruthy();
        });
        return it("should not show an error if controlValue is set", function() {
          this.bnc.$('.bv_controlValue').val("");
          this.bnc.$('.bv_controlValue').change();
          this.bnc.$('.bv_controlValue').val(" batchName1 ");
          this.bnc.$('.bv_controlValue').change();
          return expect(this.bnc.$('.bv_controlValue').hasClass('error')).toBeFalsy();
        });
      });
    });
    describe("PNControlList Controller", function() {
      return describe("when instantiated", function() {
        beforeEach(function() {
          this.bnl = new PNControlList([
            {
              controlName: "controlName1",
              controlValue: "controlValue1"
            }, {
              controlName: "controlName2",
              controlValue: "controlValue2"
            }, {
              controlName: "controlName3",
              controlValue: ""
            }
          ]);
          this.bnlc = new PNControlListController({
            collection: this.bnl,
            el: this.fixture
          });
          return this.bnlc.render();
        });
        it("should have as many batch divs as in collection", function() {
          return expect(this.bnlc.$("tr").length).toEqual(3);
        });
        it("should remove list entry when a remove button clicked", function() {
          this.bnlc.$(".PNControlView:eq(1) .bv_controlButton").click();
          expect(this.bnlc.$("tr").length).toEqual(2);
          return expect(this.bnlc.collection.length).toEqual(2);
        });
        it("should show new PNControlView when new added to existing list", function() {
          expect(this.bnlc.$("tr").length).toEqual(3);
          this.bnlc.collection.add([
            {
              controlName: "controlValue4",
              controlValue: "controlValue4"
            }, {
              controlName: "controlValue5",
              controlValue: "controlValue5"
            }
          ]);
          return expect(this.bnlc.$("tr").length).toEqual(5);
        });
        return it("should update view if a members control value is changed", function() {
          expect(this.bnlc.$(".bv_controlValue:eq(1)").val()).toEqual("controlValue2");
          this.bnlc.collection.at(1).set({
            controlValue: "fred"
          });
          return expect(this.bnlc.$(".bv_controlValue:eq(1)").val()).toEqual("fred");
        });
      });
    });
    describe('EchoPlateMapper Model', function() {
      describe('when instantiated', function() {
        beforeEach(function() {
          return this.echoPlateMapper = new EchoPlateMapper();
        });
        return describe("defaults tests", function() {
          return it('should have defaults', function() {
            expect(this.echoPlateMapper.get('plateSize')).toEqual(0);
            expect(this.echoPlateMapper.get('positiveControlList')).toEqual(null);
            return expect(this.echoPlateMapper.get('negitiveControlList')).toEqual(null);
          });
        });
      });
      return describe("validation tests", function() {
        beforeEach(function() {
          return this.echoPlateMapper = new EchoPlateMapper(window.EchoPlateMapperTestJSON.validEchoPlateMapper);
        });
        it("should be valid as initialized", function() {
          return expect(this.echoPlateMapper.isValid()).toBeTruthy();
        });
        return it('should have the file name be "unassigned"', function() {
          var filtErrors;

          this.echoPlateMapper.set({
            plateSize: "Insert Plate Size"
          });
          expect(this.echoPlateMapper.isValid()).toBeFalsy();
          filtErrors = _.filter(this.echoPlateMapper.validationError, function(err) {
            return err.attribute === 'plateSize';
          });
          return expect(filtErrors.length).toBeGreaterThan(0);
        });
      });
    });
    return describe('EchoPlateMapper Controller', function() {
      return describe('when instantiated', function() {
        beforeEach(function() {
          this.epmc = new EchoPlateMapperController({
            model: new EchoPlateMapper(),
            el: $('#fixture')
          });
          return this.epmc.render();
        });
        describe("basic existance tests", function() {
          it('should exist', function() {
            return expect(this.epmc).toBeDefined();
          });
          return it('should load a template', function() {
            return expect(this.epmc.$('.bv_plateSize').length).toEqual(1);
          });
        });
        describe("it should show a text input for Plate Size", function() {
          return it("should default to unassigned", function() {
            return expect(this.epmc.$('.bv_plateSize').val()).toEqual("");
          });
        });
        describe("it should show a button for add Positive Control", function() {
          return it(' should have a add Positive Control button', function() {
            return expect(this.epmc.$('.bv_addPositive').is('button')).toBeTruthy();
          });
        });
        describe("it should show a button for add Negitive Control", function() {
          return it(' should have a add Negitive Control button', function() {
            return expect(this.epmc.$('.bv_addNegitive').is('button')).toBeTruthy();
          });
        });
        describe("it should show a button for generate echo plate mapper", function() {
          return it(' should have a generate echo plate mapper button', function() {
            return expect(this.epmc.$('.bv_generateEchoPlateMapper').is('button')).toBeTruthy();
          });
        });
        return describe("it should show add a control when click add button", function() {
          return it(' should show add a control when click add button', function() {
            console.log(this.epmc.$('.bv_addNegitive'));
            this.epmc.$('.bv_addNegitive').click();
            console.log(this.epmc);
            return expect(this.epmc.$('.bv_addNegitive').is('button')).toBeFalsy();
          });
        });
      });
    });
  });

}).call(this);
