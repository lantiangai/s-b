(function() {
  (function(exports) {
    exports.validEchoPlateMapper = {
      file: "Echo Plate Mapper",
      plateSize: 20,
      positiveControlList: [
        {
          "test1": "test1",
          "1111": "1111"
        }, {
          "test2": "test2",
          "2222": "2222"
        }
      ],
      nagitiveControlList: [
        {
          "test3": "test3",
          "3333": "3333"
        }
      ]
    };
    exports.returnExampleSuccess = {
      transactionId: -1,
      results: {
        path: "path/to/file",
        fileToParse: "filename.xls",
        reportFile: null,
        htmlSummary: "HTML from service",
        dryRun: true
      },
      hasError: false,
      hasWarning: true,
      errorMessages: []
    };
    return exports.validPNControl = {
      controlName: "test1",
      controlValue: "value1"
    };
  })((typeof process === "undefined" || !process.versions ? window.EchoPlateMapperTestJSON = window.EchoPlateMapperTestJSON || {} : exports));

}).call(this);
