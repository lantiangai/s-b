((exports) ->
	exports.validEchoPlateMapper =
		file: "Echo Plate Mapper"
		plateSize: 20
		positiveControlList: [{"test1","1111"},{"test2","2222"}]
		nagitiveControlList: [{"test3","3333"}]

	exports.returnExampleSuccess =
		transactionId: -1
		results:
			path: "path/to/file"
			fileToParse: "filename.xls"
			reportFile: null #if user uploads report, put temp path here
			htmlSummary: "HTML from service"
			dryRun: true
		hasError: false
		hasWarning: true
		errorMessages: []

	exports.validPNControl =
		controlName:"test1"
		controlValue:"value1"

) (if (typeof process is "undefined" or not process.versions) then window.EchoPlateMapperTestJSON = window.EchoPlateMapperTestJSON or {} else exports)

