describe 'EchoPlateMapper Behavior Testing', ->

	beforeEach ->
		@fixture = $.clone($('#fixture').get(0))

	afterEach ->
		$('#fixture').remove()
		$('body').append $(this.fixture)

	describe 'PNControl Model', ->
		describe 'when instantiated', ->
			beforeEach ->
				@pnc = new PNControl()
			describe "defaults tests", ->
				it  'should have defaults', ->
					expect(@pnc.get 'controlName').toEqual ""
					expect(@pnc.get 'controlValue').toEqual ""
		describe "validation tests", ->
			beforeEach ->
				@pnc = new PNControl window.EchoPlateMapperTestJSON.validPNControl

			it "should be valid as initialized", ->
				expect(@pnc.isValid()).toBeTruthy()

			it 'should have the controlName be "unassigned"', ->
				@pnc.set
					controlName: ""
					controlValue: ""
				expect(@pnc.isValid()).toBeFalsy()
				filtErrors = _.filter(@pnc.validationError, (err) ->
					err.attribute=='control valid'
				)
				expect(filtErrors.length).toBeGreaterThan 0


	describe "PNControlList Model", ->
		describe "when instantiated", ->
			beforeEach ->
				@bnl = new PNControlList([
					controlName: "reqName1"
					controlValue: "batchName1"
				,
					controlName: "reqName2"
					controlValue: "aliasName2"
				,
					controlName: ""
					controlValue: ""
				])
			it "should have three PNControl when instantiated with 3 models", ->
				expect(@bnl.length).toEqual 3
			it "should return set list of valid models", ->
				expect(@bnl.getValidBatchNames().length).toEqual 2
			it "should be invalid when has any invalid PNCotrol", ->
				expect(@bnl.isValid()).toBeFalsy()
			it "should not add models that are the same", ->
				expect(@bnl.length).toEqual 3
				@bnl.add
					controlName: "reqName2"
					controlValue: "aliasName2"
				@bnl.add
					controlName: "reqName2other"
					controlValue: "aliasName2"
				expect(@bnl.length).toEqual 4
			it "should be valid when all PNControl valid", ->
				@bnl.pop()
				expect(@bnl.isValid()).toBeTruthy()

	describe "PNControl Controller", ->
		describe "when instantiated", ->
			beforeEach ->
				@bnc = new PNControlController(
					model: new PNControl(
						controlName: "reqName1"
						controlValue: "batchName1"
					)
					el: @fixture
				)
				@bnc.render()
			it "should show the control name", ->
				expect(@bnc.$(".bv_controlValue").val()).toEqual "batchName1"
			it "should show an error if controlValue is not set", ->
				@bnc.$('.bv_controlValue').val("")
				@bnc.$('.bv_controlValue').change()
				expect( @bnc.$('.bv_controlValue').hasClass('error')).toBeTruthy()
			it "should not show an error if controlValue is set", ->
				@bnc.$('.bv_controlValue').val("")
				@bnc.$('.bv_controlValue').change()
				@bnc.$('.bv_controlValue').val(" batchName1 ")
				@bnc.$('.bv_controlValue').change()
				expect( @bnc.$('.bv_controlValue').hasClass('error')).toBeFalsy()

	describe "PNControlList Controller", ->
		describe "when instantiated", ->
			beforeEach ->
				@bnl = new PNControlList [
					controlName: "controlName1"
					controlValue: "controlValue1"
				,
					controlName: "controlName2"
					controlValue: "controlValue2"
				,
					controlName: "controlName3"
					controlValue: ""
				]
				@bnlc = new PNControlListController(
					collection: @bnl
					el: @fixture
				)
				@bnlc.render()
			it "should have as many batch divs as in collection", ->
				expect(@bnlc.$("tr").length).toEqual 3

			it "should remove list entry when a remove button clicked", ->
				@bnlc.$(".PNControlView:eq(1) .bv_controlButton").click()
				expect(@bnlc.$("tr").length).toEqual 2
				expect(@bnlc.collection.length).toEqual 2

			it "should show new PNControlView when new added to existing list", ->
				expect(@bnlc.$("tr").length).toEqual 3
				@bnlc.collection.add [
					controlName: "controlValue4"
					controlValue: "controlValue4"
				,
					controlName: "controlValue5"
					controlValue: "controlValue5"
				]
				expect(@bnlc.$("tr").length).toEqual 5

			it "should update view if a members control value is changed", ->
				expect(@bnlc.$(".bv_controlValue:eq(1)").val()).toEqual "controlValue2"
				@bnlc.collection.at(1).set controlValue: "fred"
				expect(@bnlc.$(".bv_controlValue:eq(1)").val()).toEqual "fred"


	describe 'EchoPlateMapper Model', ->
		describe 'when instantiated', ->
			beforeEach ->
				@echoPlateMapper = new EchoPlateMapper()
			describe "defaults tests", ->
				it  'should have defaults', ->
					expect(@echoPlateMapper.get 'plateSize').toEqual 0
					expect(@echoPlateMapper.get 'positiveControlList').toEqual null
					expect(@echoPlateMapper.get 'negitiveControlList').toEqual null
		describe "validation tests", ->
			beforeEach ->
				@echoPlateMapper = new EchoPlateMapper window.EchoPlateMapperTestJSON.validEchoPlateMapper

			it "should be valid as initialized", ->
				expect(@echoPlateMapper.isValid()).toBeTruthy()

			it 'should have the file name be "unassigned"', ->
				@echoPlateMapper.set
					plateSize: "Insert Plate Size"
				expect(@echoPlateMapper.isValid()).toBeFalsy()
				filtErrors = _.filter(@echoPlateMapper.validationError, (err) ->
					err.attribute=='plateSize'
				)
				expect(filtErrors.length).toBeGreaterThan 0

	describe 'EchoPlateMapper Controller', ->
		describe 'when instantiated', ->
			beforeEach ->
				@epmc = new EchoPlateMapperController
					model: new EchoPlateMapper()
					el: $('#fixture')
				@epmc.render()
			describe "basic existance tests", ->
				it 'should exist', ->
					expect(@epmc).toBeDefined()
				it 'should load a template', ->
					expect(@epmc.$('.bv_plateSize').length).toEqual 1
			describe "it should show a text input for Plate Size", ->
				it "should default to unassigned", ->
					expect(@epmc.$('.bv_plateSize').val()).toEqual ""
			describe "it should show a button for add Positive Control", ->
				it ' should have a add Positive Control button', ->
					expect(@epmc.$('.bv_addPositive').is('button')).toBeTruthy()
			describe "it should show a button for add Negitive Control", ->
				it ' should have a add Negitive Control button', ->
					expect(@epmc.$('.bv_addNegitive').is('button')).toBeTruthy()
			describe "it should show a button for generate echo plate mapper", ->
				it ' should have a generate echo plate mapper button', ->
					expect(@epmc.$('.bv_generateEchoPlateMapper').is('button')).toBeTruthy()
			describe "it should show add a control when click add button", ->
				it ' should show add a control when click add button', ->
					console.log @epmc.$('.bv_addNegitive')
					@epmc.$('.bv_addNegitive').click()
					console.log @epmc
					expect(@epmc.$('.bv_addNegitive').is('button')).toBeFalsy()