class window.PNControl extends Backbone.Model
	defaults:
		controlName:""
		controlValue:""

	getDisplayName: ->
		if @hasValidName()
			@get "controlName"

	validate: (attrs) ->
		errors = []
		if attrs.controlName is "" or attrs.controlValue is ""
			errors.push
				attribute: 'control valid'
				message: "Control Name and Value must be valid"

		if errors.length > 0
			return errors
		else
			return null

	isSame: (other) ->
		return true  if @get("controlName") isnt "" and @get("controlName") is other.get("controlName")
		return true  if @get("controlName") is "" and other.get("controlName") is ""
		false

	hasValidValue: ->
		@get("controlValue") isnt ""
	hasValidName: ->
		@get("controlName") isnt ""

class window.PNControlList extends Backbone.Collection
	model:PNControl

	getValidBatchNames: ->
		@filter (nm) ->
			nm.isValid()

	add: (model, options) ->
		if model instanceof Array
			_.each model, (mdl) =>
				@add mdl, options
		else
			isDupe = @any (tmod) ->
				tmod.isSame new PNControl(model)

			return false  if isDupe
			Backbone.Collection::add.call this, model

	isValid: =>
		if (@getValidBatchNames().length == @length) and (@length != 0)
			return true
		else
			return false

	clear: ->
		@destroy()

class window.PNControlController extends Backbone.View
	template: _.template($("#PNControlView").html())
	tagName: "tr"
	className: "pnControlView"

	events:
		'change .bv_controlName': "updateControl"
		'change .bv_controlValue': "updateControl"
		'click .bv_controlButton': "deleteControl"

	initialize: ->
		@model.on "change", @render, @
		@model.on "destroy", @remove, @
#		@setBindings()

	render: =>
		$(@el).html @template()
		@$(".bv_controlName").val @model.get "controlName"
		@$(".bv_controlValue").val @model.get "controlValue"
		unless @model.hasValidName()
			@$('.bv_controlName').addClass "error"
		else
			@$('.bv_controlName').removeClass "error"
		unless @model.hasValidValue()
			@$('.bv_controlValue').addClass "error"
		else
			@$('.bv_controlValue').removeClass "error"
		@
	deleteControl:=>
		console.log "click delete"
		@model.clear()

	updateControl: =>
		@model.set
			controlName: @$('.bv_controlName').find(":selected").text()
			controlValue: $.trim(@$('.bv_controlValue').val())

class window.PNControlListController extends Backbone.View
	template: _.template($("#PNControlListView").html())

	initialize: ->
		@collection.bind "add", @add, @

	render: ->
		$(@el).empty()
		$(@el).html @template()
		@collection.each (bName) =>
			@$('tbody').append new PNControlController(model: bName).render().el
		this

	add: =>
		@render()

class window.EchoPlateMapper extends Backbone.Model
	defaults:
		plateSize: 0
		positiveControlList: null
		negitiveControlList: null

	validate: (attrs) ->
		errors = []
		if  attrs.plateSize == "Insert Plate Size"
			errors.push
				attribute: 'plateSize'
				message: "Plate Size must be provided"
		if errors.length > 0
			return errors
		else
			return null

class window.EchoPlateMapperController extends AbstractFormController
	template: _.template($("#EchoPlateMapperView").html())

	events:
		'change .bv_plateSize': "attributeChanged"
		'click .bv_addPositive': "addPositive"
		'click .bv_addNegitive': "addNegitive"

	initialize: ->
		@errorOwnerName = 'EchoPlateMapperController'
		$(@el).html @template()
		@positiveControlList =new PNControlListController
			collection: new PNControlList()
			el: @$(".positiveControlContainer")
		@negitiveControlList =new PNControlListController
			collection: new PNControlList()
			el: @$(".negitiveControlContainer")

	render: =>
		
		this

	attributeChanged: =>
		@trigger 'amDirty'
		@updateModel()

	updateModel: ->
		@.set
			plateSize: @getTrimmedInput('.plateSize')

	addPositive: =>
		event.preventDefault();
		@positiveControlList.add(new PNControl())
		console.log "done"
		console.log @positiveControlList

	addNegitive: =>
		event.preventDefault();
		@negitiveControlList.add(new PNControl())

class window.EchoPlateMapperParserController extends BasicFileValidateAndSaveController

	initialize: ->
		@fileProcessorURL = "/api/echoPlateMapperParser"
		@errorOwnerName = 'EchoPlateMapperParser'
		@loadReportFile = false
		super()
		@$('.bv_moduleTitle').html('Full PK Experiment Loader')
		@fpkc = new EchoPlateMapperController
			model: new EchoPlateMapper()
			el: @$('.bv_additionalValuesForm')
		@fpkc.on 'valid', @handleFPKFormValid
		@fpkc.on 'invalid', @handleFPKFormInvalid
		@fpkc.on 'notifyError', @notificationController.addNotification
		@fpkc.on 'clearErrors', @notificationController.clearAllNotificiations
		@fpkc.render()

	handleFPKFormValid: =>
		if @parseFileUploaded
			@handleFormValid()

	handleFPKFormInvalid: =>
		@handleFormInvalid()

	handleFormValid: ->
		if @fpkc.isValid()
			super()

	handleValidationReturnSuccess: (json) =>
		super(json)
		@fpkc.disableAllInputs()

	showFileSelectPhase: ->
		super()
		if @fpkc?
			@fpkc.enableAllInputs()

	validateParseFile: =>
		@fpkc.updateModel()
		unless !@fpkc.isValid()
			@additionalData =
				inputParameters: @fpkc.model.toJSON()
			super()

	validateParseFile: =>
		@fpkc.updateModel()
		unless !@fpkc.isValid()
			@additionalData =
				inputParameters: @fpkc.model.toJSON()
			super()
