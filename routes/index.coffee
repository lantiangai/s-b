
requiredScripts = [
	'src/lib/jquery.min.js'
	'src/lib/json2.js'
	'src/lib/underscore.js'
	'src/lib/backbone-min.js'
	'/src/lib/bootstrap/bootstrap-tooltip.js'
	'/src/lib/jqueryFileUpload/js/vendor/jquery.ui.widget.js'
	'/src/lib/jqueryFileUpload/js/jquery.iframe-transport.js'
	'/src/lib/bootstrap/bootstrap.min.js'
	'/src/lib/jqueryFileUpload/tmpl.min.js'
	'/src/lib/jqueryFileUpload/js/jquery.iframe-transport.js'
	'/src/lib/jqueryFileUpload/js/jquery.fileupload.js'
	'/src/lib/jqueryFileUpload/js/jquery.fileupload-fp.js'
	'/src/lib/jqueryFileUpload/js/jquery.fileupload-ui.js'
	'/src/lib/jqueryFileUpload/js/locale.js'
	'/src/lib/jquery-ui-1.10.2.custom/js/jquery-ui-1.10.2.custom.min.js'
]


applicationScripts = [
	'src/conf/configuration.js'
	'src/conf/configurationNode.js'
	'/javascripts/src/LSFileInput.js'
	'/javascripts/src/LSFileChooser.js'
	'/javascripts/src/LSErrorNotification.js'
	'/javascripts/src/AbstractFormController.js'
	'/javascripts/src/BasicFileValidateAndSaveController.js'
	# For EchoPlateMapper module
	'/javascripts/src/EchoPlateMapper.js'
]

exports.index = (req, res) ->
	"use strict"
	global.specRunnerTestmode = false
	scriptsToLoad = requiredScripts.concat(applicationScripts)
	res.render('index', {
		title: 'EchoPlateMapper',
		scripts: scriptsToLoad
	})

exports.specRunner = (req, res) ->
	"use strict"
	jasmineScripts = [
		'src/lib/testLibraries/jasmine-jstd-adapter/jasmine/lib/jasmine-core/jasmine.js',
		'src/lib/testLibraries/jasmine-jstd-adapter/jasmine/lib/jasmine-core/jasmine-html.js',
		'src/lib/testLibraries/jasmine-jquery/lib/jasmine-jquery.js',
		'src/lib/testLibraries/sinon.js'
	]

	specScripts = [
		# For EchoPlateMapper module
		'javascripts/spec/testFixtures/EchoPlateMapperTestJSON.js'
		'javascripts/spec/EchoPlateMapperSpec.js'
		'javascripts/spec/EchoPlateMapperParserServiceSpec.js'
	]

	scriptsToLoad = requiredScripts.concat(jasmineScripts, specScripts)
	scriptsToLoad = scriptsToLoad.concat(applicationScripts)
	global.specRunnerTestmode = true
	res.render('SpecRunner', {
		title: 'EchoPlateMapper SpecRunner',
		scripts: scriptsToLoad
	})
